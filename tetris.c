#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include "rutines.h"

#define Files 29 //N�mero de files del nostre tauler virtual
#define Columnes 20 //N�mero de columnes del nostre tauler virtual
#define N_Peces 7 //N�mero de peces que existeixen
#define ESC 27 //Valor del car�cter de la tecla escape
#define Espai 32 //Valor del car�cter de la tecla espai
#define Adalt 72  //Valor del car�cter de la tecla fletxa amunt
#define Dreta 77 //Valor del car�cter de la tecla fletxa dreta
#define Esquerre 75 //Valor del car�cter de la tecla fletxa esquerre
#define Abaix_P 80 //Valor del car�cter de la tecla fletxa abaix o de la tecla P
#define Dimensio 3 //Dimensi� de la matriu de les peces
#define KEY_SPECIAL 224 //Valor especial que tenim com a refer�ncia per distingir la tecla fletxa abaix i la tecla P
#define GotCol 28 //Valor de refer�ncia de les columnes a l'hora de dibuixar les peces al tauler
#define GotFil 5 //Valor de refer�ncia de les files a l'hora de dibuixar les peces al tauler
#define principiant 750
#define mitja 500
#define expert 250

/*Amb aquest procediment el que fem es inicialitzar la matriu de la pe�a i despres a partir d'un n�mero aleatori generat a una
altra funci� posem el valors a la matriu de la peces
*/

void Crear_Peca (int aleatori, int peca[Dimensio][Dimensio])
{
	int i, j;
	
	for (i = 0; i < Dimensio; ++i)
	{
		for (j = 0; j < Dimensio; ++j)
		{
			peca[i][j] = 0;
		}
	}

	switch (aleatori)
	{
		case 0: 
			peca[1][0] = CYAN;
			peca[1][1] = CYAN;
			peca[1][2] = CYAN;
			break;
		case 1:
			peca[1][0] = BLUE;
			peca[2][0] = BLUE;
			peca[2][1] = BLUE;
			peca[2][2] = BLUE;
			break;
		case 2:
			peca[1][2] = BROWN;
			peca[2][0] = BROWN;
			peca[2][1] = BROWN;
			peca[2][2] = BROWN;
			break;
		case 3:
			peca[0][0] = YELLOW;
			peca[0][1] = YELLOW;
			peca[1][0] = YELLOW;
			peca[1][1] = YELLOW;
			break;
		case 4:
			peca[1][1] = GREEN;
			peca[1][2] = GREEN;
			peca[2][0] = GREEN;
			peca[2][1] = GREEN;
			break;
		case 5:
			peca[1][1] = MAGENTA;
			peca[2][0] = MAGENTA;
			peca[2][1] = MAGENTA;
			peca[2][2] = MAGENTA;
			break;
		case 6:
			peca[1][0] = RED;
			peca[1][1] = RED;
			peca[2][1] = RED;
			peca[2][2] = RED;
			break;
	}
}

//Funci� que genera un n�mero aleatori i crida el procediment que crea la pe�a

int Generar_aleatori (int peca [Dimensio][Dimensio])
{
	int aleatori;

	aleatori = rand()%(N_Peces);

	Crear_Peca (aleatori, peca);

	return aleatori;
}

//Funci� que detecta si hi ha algun obstacle per saber on hem de parar la baixada

int xoc_abaix (int tauler_virtual [Files][Columnes], int peca [Dimensio][Dimensio], TipusPosicio p)
{
	int trobat, i, j;
	TipusPosicio aux;

	trobat = 0;
	
	while (trobat == 0 || p.f == Files)
	{
		aux.f = p.f;
		for (i = 0; i < Dimensio; ++i)
		{
			aux.c = p.c;
			for (j = 0; j < Dimensio; ++j)
			{
				if (peca[i][j] != 0)
				{
					//Detecta la fila i la columna on hi ha el primer obstacle
					if (tauler_virtual [aux.f + 1][aux.c] != 0) trobat = 1;
				}
				++aux.c;
			}
			++aux.f;
		}
		++p.f;
	}
	if (trobat == 1) return aux.f;	//Retorna la fila on hi ha obstacle
}

//Funci� que detecta si hi ha algun obstacle per saber on hem de parar al anar cap a la dreta

int xoc_dreta (int tauler_virtual [Files][Columnes], int peca [Dimensio][Dimensio], TipusPosicio p)
{
	int trobat, i, j;
	TipusPosicio aux;

	trobat = 0;
	aux.f = p.f;
	
	for (i = 0; i < Dimensio; ++i)
	{
		aux.c = p.c;
		for (j = 0; j < Dimensio; ++j)
		{
			if (peca[i][j] != 0)
			{
				if (tauler_virtual[aux.f][aux.c + 1] != 0) trobat = 1;
			}
			++aux.c;
		}
		++aux.f;
	}
	return trobat;
}

//Funci� que detecta si hi ha algun obstacle per saber on hem de parar al anar cap a l'esquerre

int xoc_esquerre (int tauler_virtual [Files][Columnes], int peca [Dimensio][Dimensio], TipusPosicio p)
{
	int trobat, i, j;
	TipusPosicio aux;

	trobat = 0;
	aux.f = p.f;
	
	for (i = 0; i < Dimensio; ++i)
	{
		aux.c = p.c;
		for (j = 0; j < Dimensio; ++j)
		{
			if (peca[i][j] != 0)
			{
				if (tauler_virtual[aux.f][aux.c - 1] != 0) 
				{
					trobat = 1;
				}
			}
			++aux.c;
		}
		++aux.f;
	}
	return trobat;
}

//Detecta si hi ha qualsevol obstacle que ens impedeixi seguir baixant

int xoc (int tauler_virtual [Files][Columnes], int peca [Dimensio][Dimensio], TipusPosicio p)
{
	int trobat, i, j;
	TipusPosicio aux;

	trobat = 0;
	aux.f = p.f;
	
	for (i = 0; i < Dimensio; ++i)
	{
		aux.c = p.c;
		for (j = 0; j < Dimensio; ++j)
		{
			if (peca[i][j] != 0)
			{
				if (tauler_virtual [aux.f + 1][aux.c] != 0) trobat = 1;
			}
			++aux.c;
		}
		++aux.f;
	}
	return trobat;
}

/*Inicialitzem amb 0 una matriu que te la mateixa mida que el tauler amb l'afegit de dos columnes que fan de top
esquerra i top dret i una fila extra que tamb� fa de top abaix */

void ini_tauler_virtual (int tauler_virtual [Files][Columnes])
{
	int i, j;

	for (i = 0; i < Files; ++i)
	{
		for (j = 0; j < Columnes; ++j)
		{
			if (j == 0 || j == Columnes - 1 || i == Files - 1) tauler_virtual [i][j] = 8; //Detecta els tops i hi possa 8
			else tauler_virtual [i][j] = 0;
		}
	}
}

//Copia la informaci� del tauler de joc al tauler virtual

void copiar_tauler_virtual (int tauler_virtual [Files][Columnes], TipusPosicio p, int peca[Dimensio][Dimensio], int aleatori)
{
	int i, j;
	TipusPosicio aux;

	aux.f = p.f;

	for (i = 0; i < Dimensio; ++i)
	{
		aux.c = p.c;

		for (j = 0; j < Dimensio; ++j)
		{
			if (peca[i][j] != 0) 
			{
				switch (aleatori)
				{
					case 0:
						//CYAN
						tauler_virtual[aux.f][aux.c] = 1;
						break;
					case 1:
						//BLUE
						tauler_virtual[aux.f][aux.c] = 2;
						break;
					case 2:
						//BROWN
						tauler_virtual[aux.f][aux.c] = 3;
						break;
					case 3:
						//YELLOW
						tauler_virtual[aux.f][aux.c] = 4;
						break;
					case 4:
						//GREEN
						tauler_virtual[aux.f][aux.c] = 5;
						break;
					case 5:
						//MAGENTA
						tauler_virtual[aux.f][aux.c] = 6;
						break;
					case 6:
						//RED
						tauler_virtual[aux.f][aux.c] = 7;
						break;
				}
			}
			++aux.c;		
		}
		++aux.f;
	}	
}

//Transposa la matriu

void Transposa (int peca [Dimensio][Dimensio], int peca_aux [Dimensio][Dimensio])
{
	int i, j;

	for (i = 0; i < Dimensio; ++i)
	{
		for (j = 0; j < Dimensio; ++j)
		{
			peca_aux [j][i] = peca [i][j];
		}
	}
}

//Reflexa la matriu

void Reflexa (int peca [Dimensio][Dimensio], int peca_aux [Dimensio][Dimensio])
{
	int i, j, k;
	int peca_aux_2 [Dimensio][Dimensio];

	for (i = 0; i < Dimensio; ++i)
	{
		k = 2;
		for (j = 0; j < Dimensio; ++j)
		{
			peca_aux_2 [i][k] = peca_aux [i][j];
			--k;
		}
	}
	for (i = 0; i < Dimensio; ++i)
	{
		for (j = 0; j < Dimensio; ++j)
		{
			peca_aux [i][j] = peca_aux_2 [i][j];
		}
	}
}

//Implementa la transposada i la reflexada excepte en el cas de que la pe�a sigui el quadrat ja que no cal rotarla

void Rota (int peca [Dimensio][Dimensio], int aleatori, int tauler_virtual [Files][Columnes], TipusPosicio p)
{
	int i, j, aux_f, aux_c, trobat;
	int peca_aux [Dimensio][Dimensio];

	aux_f = p.f;
	aux_c = p.c;
	trobat = 0;

	for (i = 0; i < Dimensio; ++i)
	{
		for (j = 0; j < Dimensio; ++j)
		{
			peca_aux [i][j] = peca [i][j];
		}
	}
	if (aleatori != 3)
	{
		Transposa (peca, peca_aux);
		Reflexa (peca, peca_aux);
	}
	for (i = 0; i < Dimensio; ++i)
	{
		aux_c = p.c;
		for (j = 0; j < Dimensio; ++j)
		{
			if (peca_aux[i][j] != 0 && tauler_virtual [aux_f][aux_c] != 0)
			{
				trobat = 1;
			}
			++aux_c;
		}
		++aux_f;
	}
	if (trobat == 0)
	{
		for (i = 0; i < Dimensio; ++i)
		{
			for (j = 0; j < Dimensio; ++j)
			{
				peca [i][j] = peca_aux [i][j];
			}
		}
	}
}

//Fa el control de totes les tecles que es poden pr�mer i retorna si hem premut la tecla de sortir

int Tecles (TipusPosicio *p, int peca [Dimensio][Dimensio], int tauler_virtual [Files][Columnes], pantalla_t tauler, int Puntuacio, int aleatori)
{
	int obstacle, teclaEspecial, i, j, fijoc;
	unsigned char tecla;

	obstacle = 0;
	fijoc = 0;

	tecla = getch();
    
	//Detecta la doble escritura
	if (tecla == KEY_SPECIAL) 
	{
		teclaEspecial = 1; 
		tecla = getch();
	}
    else teclaEspecial = 0;
		
	fflush(stdin);

	switch (tecla)
	{
	case 'p':
	case Abaix_P:
		if (teclaEspecial == 1) //Implementem les funcions per anar cap abaix
        {
			EsborraPesa(&tauler, peca, p);
			p->f = xoc_abaix (tauler_virtual, peca, (*p)) - 3;
			MostraPesa (&tauler, peca, p);
        }
		else
		{
			//Fem la pausa
			MostraMSG (&tauler ,"PAUSA");
			system ("cls");
			MostraTauler(&tauler);
			MostraPartida (&tauler, "Jugador", Puntuacio);

			//Al fer la pausa, hem de dibuixar totes les peces que hi havia abans al tauler
			for (i = 0; i < Files; ++i)
			{
				for (j = 0; j < Columnes; ++j)
				{
					if (tauler_virtual[i][j] != 0)
					{
						switch (tauler_virtual[i][j])
						{
							case 1:
								gotoxy(GotCol + (j - 1),GotFil + i,tauler.screen);			
								TextColor(CYAN,BLACK,tauler.screen);
								printf("%c",177);
								break;
							case 2:
								gotoxy(GotCol + (j - 1),GotFil + i,tauler.screen);			
								TextColor(BLUE,BLACK,tauler.screen);
								printf("%c",177);
								break;
							case 3:
								gotoxy(GotCol + (j - 1),GotFil + i,tauler.screen);			
								TextColor(BROWN,BLACK,tauler.screen);
								printf("%c",177);
								break;
							case 4:
								gotoxy(GotCol + (j - 1),GotFil + i,tauler.screen);			
								TextColor(YELLOW,BLACK,tauler.screen);
								printf("%c",177);
								break;
							case 5:
								gotoxy(GotCol + (j - 1),GotFil + i,tauler.screen);			
								TextColor(GREEN,BLACK,tauler.screen);
								printf("%c",177);
								break;
							case 6:
								gotoxy(GotCol + (j - 1),GotFil + i,tauler.screen);			
								TextColor(MAGENTA,BLACK,tauler.screen);
								printf("%c",177);
								break;
							case 7:
								gotoxy(GotCol + (j - 1),GotFil + i,tauler.screen);			
								TextColor(RED,BLACK,tauler.screen);
								printf("%c",177);
								break;
						}	
					}
				}
			}
		}
		break;
	case Esquerre: //Implementem les funcions per anar cap a l'esquerre
		obstacle = xoc_esquerre (tauler_virtual, peca, (*p));
		if (obstacle == 0) 
		{
			EsborraPesa(&tauler, peca, p);
			--p->c;
			MostraPesa (&tauler, peca, p);
		}
		break;
	case Dreta: //Implementem les funcions per anar cap a la dreta
		obstacle = xoc_dreta (tauler_virtual, peca, (*p));
		if (obstacle == 0) 
		{
			EsborraPesa(&tauler, peca, p);
			++p->c;
			MostraPesa (&tauler, peca, p);
		}
		break;
	case ESC: //Sortim del joc
		fijoc = 1;
		break;
	case Adalt://Rotem la pe�a
	case Espai:
		EsborraPesa(&tauler, peca, p);
		Rota (peca, aleatori, tauler_virtual, (*p));
		MostraPesa (&tauler, peca, p);
		break;
	}
	return fijoc;
}

//Comprovem la fila 1 de la pe�a per veure si es completa 

int comprova_linia_1 (int tauler_virtual [Files][Columnes], TipusPosicio p)
{
	int j, completa, linia;

	completa = 1;
	linia = p.f;

	for (j = 0; j < Columnes; ++j)
	{
		if (tauler_virtual [linia][j] == 0) completa = 0;
	}
	return completa;
}

//Comprovem la fila 2 de la pe�a per veure si es completa

int comprova_linia_2 (int tauler_virtual [Files][Columnes], TipusPosicio p)
{
	int j, completa, linia;

	completa = 1;
	linia = p.f + 1;

	for (j = 0; j < Columnes; ++j)
	{
		if (tauler_virtual [linia][j] == 0) completa = 0;
	}
	return completa;
}

//Comprovem la fila 3 de la pe�a per veure si es completa

int comprova_linia_3 (int tauler_virtual [Files][Columnes], TipusPosicio p)
{
	int j, completa, linia;

	completa = 1;
	linia = p.f + 2;

	for (j = 0; j < Columnes; ++j)
	{
		if (tauler_virtual [linia][j] == 0) completa = 0;
	}
	return completa;
}

//Baixem totes le peces que hi havia per sobre de la l�nia eliminada

void Baixar_Blocs (int tauler_virtual [Files][Columnes], int fila, pantalla_t tauler, int Puntuacio)
{
	int i, j;
	
	for (i = fila; i > 0; --i)
	{
		for (j = 1; j < Columnes - 1; ++j)
		{
			tauler_virtual [i][j] = tauler_virtual [i - 1][j]; //Anem copiant el que hi havia a les files anteriors del tauler virtual per anar baixant
		}
	}

	system ("cls");
	MostraTauler(&tauler);
	MostraPartida (&tauler, "Jugador", Puntuacio);
	for (i = 0; i < Files; ++i) //Dibuixem totes les files al tauler real
	{
		for (j = 0; j < Columnes; ++j)
		{
			if (tauler_virtual[i][j] != 0)
			{
				switch (tauler_virtual[i][j])
				{
					case 1:
						gotoxy(GotCol + (j - 1),GotFil + i,tauler.screen);			
						TextColor(CYAN,BLACK,tauler.screen);
						printf("%c",177);
						break;
					case 2:
						gotoxy(GotCol + (j - 1),GotFil + i,tauler.screen);			
						TextColor(BLUE,BLACK,tauler.screen);
						printf("%c",177);
						break;
					case 3:
						gotoxy(GotCol + (j - 1),GotFil + i,tauler.screen);			
						TextColor(BROWN,BLACK,tauler.screen);
						printf("%c",177);
						break;
					case 4:
						gotoxy(GotCol + (j - 1),GotFil + i,tauler.screen);			
						TextColor(YELLOW,BLACK,tauler.screen);
						printf("%c",177);
						break;
					case 5:
						gotoxy(GotCol + (j - 1),GotFil + i,tauler.screen);			
						TextColor(GREEN,BLACK,tauler.screen);
						printf("%c",177);
						break;
					case 6:
						gotoxy(GotCol + (j - 1),GotFil + i,tauler.screen);			
						TextColor(MAGENTA,BLACK,tauler.screen);
						printf("%c",177);
						break;
					case 7:
						gotoxy(GotCol + (j - 1),GotFil + i,tauler.screen);			
						TextColor(RED,BLACK,tauler.screen);
						printf("%c",177);
						break;
				}	
			}
		}
	}

}

//Esborrem la l�nia corresponent del tauler virtual

void Esborra_Linia_Virtual (int tauler_virtual [Files][Columnes], int fila)
{
	int j;

	for (j = 1; j < Columnes - 1; ++j)
	{
		tauler_virtual[fila][j] = 0;
	}
}

void main ()
{
	int peca [Dimensio][Dimensio];
	int tauler_virtual [Files][Columnes];
	int Puntuacio, Puntuacio_aux, nivell, sortir, game_over, nova, aleatori, colisio, fijoc, linia_completa_1, linia_completa_2, linia_completa_3, fila_1, fila_2, fila_3;
	char opcio, opcio_sub;
	pantalla_t tauler;
	TipusPosicio p;

	sortir = 0;
	opcio = '0';
	nivell = mitja;

	while (sortir == 0)
	{
		system("cls");
		TextColor(LIGHTGREY,BLACK,tauler.screen);
		printf ("TETRIS" "\n \n" "MENU PRINCIPAL" "\n \n" "1. Puntuacions previes" "\n \n" "2. Nivell de dificultat" "\n \n" "3. Jugar" "\n \n" "4. Sortir");
		opcio = getch();
		switch (opcio)
		{
			case '1':
				system("cls");
				TextColor(LIGHTGREY,BLACK,tauler.screen);
				printf("PUNTUACIONS PREVIES" "\n \n \n \n \n \n" "Prem qualsevol tecla per tornar al Menu Principal");
				opcio_sub = getch();
			break;
			case '2':
				system("cls");
				TextColor(LIGHTGREY,BLACK,tauler.screen);
				printf("NIVELL DE DIFICULTAT" "\n \n" "1. Principiant" "\n \n" "2. Mitja" "\n \n" "3. Expert" "\n \n" "4. Torna al Menu Principal");
				opcio_sub = getch();
				switch (opcio_sub)
				{
					case '1':
						nivell = principiant;
					break;
					case '2':
						nivell = mitja;
					break;
					case '3':
						nivell = expert;
					break;
					case '4':
					break;
				}
			break;
			case '3':
				system("cls");
				InitPantalla (&tauler);
				MostraTauler(&tauler);
				
				nova = 1;
				game_over = 0;
				Puntuacio = 0;
				Puntuacio_aux = 0;
				colisio = 0;
				fijoc = 0;

				ini_tauler_virtual (tauler_virtual);

				MostraPartida (&tauler, "Jugador", Puntuacio);

				while (fijoc != 1)
				{
					
					if (nova == 1) //Si s'ha de generar una nova pe�a
					{
						p.f = 0;
						p.c = Columnes/2 - 1;
						aleatori = Generar_aleatori (peca);
						MostraPesa (&tauler, peca, &p);
						nova = 0;
					}
					while (_kbhit() != 0) //Mentre premem alguna tecla
					{
						fijoc = Tecles (&p, peca, tauler_virtual, tauler, Puntuacio, aleatori);	
					}
					if (fijoc == 1) 
					{
						MostraMSG(&tauler, "GAME OVER");
						nivell = mitja;
					}
					colisio = xoc (tauler_virtual, peca, p);
					if (colisio == 0) //Si no hi ha colisi�, anem baixant
					{
						Sleep(nivell);				
						EsborraPesa(&tauler, peca, &p);
						++p.f;
						MostraPesa (&tauler, peca, &p);
					}
					else
					{
						if (p.f == 0) //Hem arribat al top perm�s
						{
							MostraMSG(&tauler, "GAME OVER");
							fijoc = 1;
							nivell = mitja;
						}
						else //Hem arribat fins el m�s abaix possible
						{
							copiar_tauler_virtual (tauler_virtual, p, peca, aleatori);

							Puntuacio += 1; //Afegim puntuacions
							Puntuacio_aux += 1;

							linia_completa_1 = 0;
							linia_completa_2 = 0;
							linia_completa_3 = 0;

							//Posem els valors corresponents a les files i les comprovem

							fila_1 = p.f; 
							fila_2 = p.f + 1;
							fila_3 = p.f + 2;
						
							linia_completa_1 = comprova_linia_1(tauler_virtual, p);
							linia_completa_2 = comprova_linia_2(tauler_virtual, p);
							linia_completa_3 = comprova_linia_3(tauler_virtual, p);
						
							if (linia_completa_1 == 1)
							{
								EsborraLinia (&tauler, fila_1);
								Puntuacio += 10;
								Puntuacio_aux += 10;
								Esborra_Linia_Virtual (tauler_virtual, fila_1);
								Baixar_Blocs (tauler_virtual, fila_1, tauler, Puntuacio);
							}

							if (linia_completa_2 == 1)
							{
								EsborraLinia (&tauler, fila_2);
								Puntuacio += 10;
								Puntuacio_aux += 10;
								Esborra_Linia_Virtual (tauler_virtual, fila_2);
								Baixar_Blocs (tauler_virtual, fila_2, tauler, Puntuacio);
							}

							if (linia_completa_3 == 1)
							{
								if (peca[2][0] != 0 || peca[2][1] != 0 || peca[2][2] != 0)
								{
									EsborraLinia (&tauler, fila_3);
									Puntuacio += 10;
									Puntuacio_aux += 10;
									Esborra_Linia_Virtual (tauler_virtual, fila_3);
									Baixar_Blocs (tauler_virtual, fila_3, tauler, Puntuacio);
								}
							}

							if (linia_completa_3 == 1 && linia_completa_3 == 1 && linia_completa_3 == 1) //Puntuaci� extra per haver completat tres files de cop
							{
								Puntuacio += 5;
								Puntuacio_aux += 5;
							}
							nova = 1; //Hem de generar una nova pe�a
							if (Puntuacio_aux >= 50 && nivell > 150) 
							{
								nivell -= 50;
								Puntuacio_aux = 0;
							}
							MostraPartida (&tauler, "Jugador", Puntuacio);
						}
					}
				}
			break;
			case '4':
				system("cls");
				TextColor(LIGHTGREY,BLACK,tauler.screen);
				printf ("Estas segur que vols sortir? S/N" "\n");
				opcio_sub = getch();
				switch (opcio_sub)
				{
					case 's':
					case 'S':
						sortir = 1;
					break;
					case 'n':
					case 'N':
					break;
				}
			break;
		}
	}
}

